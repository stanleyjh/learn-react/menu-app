# [Menu](https://stanleyjh.gitlab.io/learn-react/menu-app/)

This application displays a food menu. There are category buttons to filter the type of food to order.

The data is referenced locally in a file called data.js. When a category is selected, `selectedCategory` is invoked and the `filter` function is used to select items within the selected category. The items matching the category are then assigned to the `updatedItems` variable and passed as an argument to the `setCategory` function to re-render the page with the specific items. If "All" is selected, setCategory is invoked with the original `items` array to show all items.

## References

[John Smilga's Udemy React Course](https://www.udemy.com/course/react-tutorial-and-projects-course/?referralCode=FEE6A921AF07E2563CEF)
