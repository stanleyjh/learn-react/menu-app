const menu = [
  {
    id: 1,
    title: "Buttermilk Pancakes",
    category: "breakfast",
    price: 15.99,
    img: "./images/item-1.jpeg",
    desc: `These fluffy pancakes are made with buttermilk, giving them a slightly tangy flavor and a soft texture. Served with your choice of syrup, butter, and toppings such as fresh reuit, whipped cream, or chocolate chips.`,
  },
  {
    id: 2,
    title: "Diner Double",
    category: "lunch",
    price: 13.99,
    img: "./images/item-2.jpeg",
    desc: `A delicious double-stacked beef burger with melted cheese, lettuce, and bacon. Served with large cut, locally grown potatoes. `,
  },
  {
    id: 3,
    title: "Godzilla Milkshake",
    category: "shakes",
    price: 6.99,
    img: "./images/item-3.jpeg",
    desc: `This decadent milkshake is a true indulgence. Made with premium ice cream, topped with whipped cream, drizzled with caramel or chocolate sauce, and finished with sprinkles or other toppings like cookies or candy.`,
  },
  {
    id: 4,
    title: "Country Delight",
    category: "breakfast",
    price: 20.99,
    img: "./images/item-4.jpeg",
    desc: `A hearty breakfast dish consisting of toasted bread, two eggs cooked your way (scrambled, fried, or poached), and a side of baked beans in a rich tomato sauce.`,
  },
  {
    id: 5,
    title: "Egg Attack",
    category: "lunch",
    price: 22.99,
    img: "./images/item-5.jpeg",
    desc: `A twist on the classic burger, this dish includes a juicy beef patty topped with a fried egg, cheddar cheese, lettuce, tomato, and mayo. Served with a side of fries.`,
  },
  {
    id: 6,
    title: "Oreo Dream",
    category: "shakes",
    price: 8.99,
    img: "./images/item-6.jpeg",
    desc: `A sweet treat made with creamy vanilla ice cream and Oreo cookies, blended until smooth and topped with whipped cream and a whole Oreo cookie.`,
  },
  {
    id: 7,
    title: "Bacon Overflow",
    category: "breakfast",
    price: 8.99,
    img: "./images/item-7.jpeg",
    desc: `Our bacon egg and cheese biscuit is layered with crispy bacon, a fried egg, and a slice of melted cheddar cheese, creating a delicious combination of flavors and textures.`,
  },
  {
    id: 8,
    title: "American Classic",
    category: "lunch",
    price: 12.99,
    img: "./images/item-8.jpeg",
    desc: `A classic American meal that includes a juicy burger patty made from ground beef, cooked to your preference and served on a hand-made bun. Accompanied by a side of crispy golden fries, salted to perfection.`,
  },
];
export default menu;
