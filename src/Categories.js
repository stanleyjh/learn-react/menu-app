import React from "react";

const Categories = (props) => {
  const { items, selectedCategory } = props;

  // (0) TASK: Create a dynamic category list from the items variable.

  // (1) Retrieve unique category values from the items variable.
  // https://stackoverflow.com/questions/15125920/how-to-get-distinct-values-from-an-array-of-objects-in-javascript
  let categories = [...new Set(items.map((item) => item.category))];

  // (2) Convert the array to an array of objects.
  const arrOfObj = [];
  for (let i = 0; i < categories.length; i++) {
    const obj = {
      id: i + 1,
      category: categories[i],
    };
    arrOfObj.push(obj);
  }
  // (3) Add the "all" category to the arrOfObj variable.
  arrOfObj.unshift({ id: 0, category: "all" });

  // (4) assign arrOfObj to categories.
  categories = arrOfObj;

  return categories.map((items) => {
    const { id, category } = items;
    return (
      <button
        type="button"
        className="filter-btn"
        key={id}
        onClick={() => {
          selectedCategory(category);
        }}
      >
        {category}
      </button>
    );
  });
};

export default Categories;
