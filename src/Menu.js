import React from "react";

const Menu = (item) => {
  const { title, price, img, desc } = item;

  return (
    <article className="menu-item">
      <img className="photo" src={img} alt={title}></img>
      <div className="item-info">
        <header>
          <h5>{title}</h5>
          <span className="item-price">${price}</span>
        </header>
        <p>{desc}</p>
      </div>
    </article>
  );
};

export default Menu;
