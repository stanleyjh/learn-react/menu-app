import React, { useState } from "react";
import Menu from "./Menu";
import Categories from "./Categories";
import items from "./data";

function App() {
  const [category, setCategory] = useState(items);

  const selectedCategory = (category) => {
    if (category !== "all") {
      const updatedItems = items.filter((item) => {
        return item.category === category;
      });
      setCategory(updatedItems);
    } else {
      setCategory(items);
    }
  };

  return (
    <main>
      <section className="menu">
        <div className="title">
          <h2>menu</h2>
          <div className="underline"></div>
        </div>
        <div className="btn-container">
          <Categories items={items} selectedCategory={selectedCategory} />
        </div>
        <div className="section-center">
          {category.map((item) => {
            return <Menu key={item.id} {...item} />;
          })}
        </div>
      </section>
    </main>
  );
}

export default App;
